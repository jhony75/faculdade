\babel@toc {brazilian}{}
\contentsline {chapter}{\numberline {1}Refer\IeC {\^e}ncias}{2}{chapter.1}% 
\contentsline {chapter}{\numberline {2}N\IeC {\'u}meros Complexos}{3}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Forma Alg\IeC {\'e}brica de um N\IeC {\'u}mero Complexo}{3}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Conjugado}{4}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Exerc\IeC {\'\i }cios}{4}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Opera\IeC {\c c}\IeC {\~o}es com N\IeC {\'u}meros Complexos}{4}{section.2.4}% 
\contentsline {subsubsection}{Adi\IeC {\c c}\IeC {\~a}o}{4}{section*.3}% 
\contentsline {paragraph}{}{4}{section*.4}% 
\contentsline {section}{\numberline {2.5}Pot\IeC {\^e}ncias de I}{4}{section.2.5}% 
\contentsline {paragraph}{ }{4}{section*.5}% 
\contentsline {chapter}{\numberline {3}Representa\IeC {\c c}\IeC {\~a}o Geom\IeC {\'e}trica de um n\IeC {\textordmasculine } Complexo}{5}{chapter.3}% 
\contentsline {chapter}{\numberline {4}M\IeC {\'o}dulos}{6}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Exerc\IeC {\'\i }cios}{6}{section.4.1}% 
\contentsline {chapter}{\numberline {5}Forma Trigonom\IeC {\'e}trica de um N\IeC {\'u}mero Complexo}{7}{chapter.5}% 
