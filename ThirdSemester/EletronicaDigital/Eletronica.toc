\babel@toc {brazilian}{}
\contentsline {chapter}{\numberline {1}Vari\IeC {\'a}veis Digitais e Anal\IeC {\'o}gicas}{II}% 
\contentsline {section}{\numberline {1.1}Vari\IeC {\'a}veis Anal\IeC {\'o}gicas}{II}% 
\contentsline {section}{\numberline {1.2}Vari\IeC {\'a}veis Digitais.}{II}% 
\contentsline {chapter}{\numberline {2}Sistemas num\IeC {\'e}ricos}{III}% 
\contentsline {section}{\numberline {2.1}Sistema Decimal}{III}% 
\contentsline {section}{\numberline {2.2}Sistema Bin\IeC {\'a}rio}{III}% 
\contentsline {subsection}{\numberline {2.2.1}Convers\IeC {\~a}o Decimal - bin\IeC {\'a}rio}{III}% 
\contentsline {section}{\numberline {2.3}Sistema de numera\IeC {\c c}\IeC {\~a}o octal}{III}% 
\contentsline {subsection}{\numberline {2.3.1}Convers\IeC {\~a}o decimal - octal}{III}% 
\contentsline {subsection}{\numberline {2.3.2}Convers\IeC {\~a}o octal - bin\IeC {\'a}rio}{III}% 
\contentsline {section}{\numberline {2.4}Sistema Hexadecimal}{IV}% 
\contentsline {subsection}{\numberline {2.4.1}Convers\IeC {\~a}o hexadecimal - decimal}{IV}% 
\contentsline {chapter}{\numberline {3}C\IeC {\'o}digos}{V}% 
\contentsline {section}{\numberline {3.1}C\IeC {\'o}digo BCD}{V}% 
\contentsline {subsection}{\numberline {3.1.1}Byte}{V}% 
\contentsline {subsection}{\numberline {3.1.2}C\IeC {\'o}digos Alfanum\IeC {\'e}ricos}{V}% 
\contentsline {section}{\numberline {3.2}Exercicios}{V}% 
\contentsline {chapter}{\numberline {4}Gates - And, Or e Not}{VI}% 
\contentsline {section}{\numberline {4.1}a)$ A * B * C + D $}{VI}% 
\contentsline {chapter}{\numberline {5}Mapa de Carnout}{VII}% 
