#include<stdio.h>

int main () {

    //Faça um algoritmo em C para ler valores até que se alcance a soma de 100. Ao atongir este valor o programa deve informar quantos valores foram digitados e a média destes

    float entrada = 0, soma = 0, media = 0;
    int contador = 0;
    
    do {
        
        printf ("Já foram inseridos %d números: \n", contador);
        printf ("Valor da soma atual: %f \n", soma);
        printf ("Insira um número: ");
        scanf ("%f", &entrada);
        
        if (entrada >= 0) {
            contador ++;
            soma = soma + entrada;
        }


    } while (soma < 100);
    
    media = soma / contador;
    printf ("Média = %f \n", media);
    printf ("Soma = %f \n", soma);

    return 0;

}