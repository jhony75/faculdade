#include <stdio.h>
 
int main() {
   char string[1000], vazio[1000];
   int i = 0, j = 0;
 
   printf("Digite uma string\n");
   fgets(string, 1000, stdin);
 
   while (string[i] != '\0') {
      if (string[i] == ' ') {
         int temp = i + 1;
         if (string[temp] != '\0') {
            while (string[temp] == ' ' && string[temp] != '\0') {
               if (string[temp] == ' ') {
                  i++;
               }  
               temp++;
            }
         }
      }
      vazio[j] = string[i];
      i++;
      j++;
   }
 
   vazio[j] = '\0';
 
   printf("String após a exclusão de espaços vazios\n%s\n", vazio);
 
   return 0;
}