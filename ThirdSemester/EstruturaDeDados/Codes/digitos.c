#include<stdio.h>

int TotalDeDigitos(int num);

int main() {
    int num;
    int totalDigitos = 0;
    printf("Insira um número: ");
    scanf("%d",&num);
    totalDigitos = TotalDeDigitos(num);
    printf("Total de números no digito = %d \n", totalDigitos);
}

int TotalDeDigitos(int num) {
    
    if(num == 0){
    return 0;
}

    return 1 + TotalDeDigitos(num/10);
}