#include <stdio.h>

float minus (float param1, float param2);

int main () {

    float valor1, valor2, resultado;

    printf ("Escreva um valor: ");
    scanf ("%f", &valor1);
    printf ("Escreva um valor: ");
    scanf ("%f", &valor2);

    resultado = minus(valor1, valor2);

    printf ("Resultado = %f\n", resultado);

    return 0;
}

float minus (float param1, float param2) {

    return param1 - param2;

}