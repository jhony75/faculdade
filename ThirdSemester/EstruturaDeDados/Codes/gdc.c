// Programa que lê 2 números e retorna o maximo divisor comum

#include <stdio.h>

int main() {

    int n1, n2;
    
    printf("Insira 2 numeros inteiros positivos: \n");
    scanf("%d %d",&n1,&n2);

    while(n1!=n2) {
        
        if(n1 > n2)
            n1 -= n2;
        else
            n2 -= n1;
    }
    
    printf("Mdc = %d \n",n1);

    return 0;
}