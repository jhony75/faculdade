#include <stdio.h>

//Protótipo da função

float notas ();

int main (void) {

    notas();
    return 0;

}

float notas () {

    float nota1, nota2, nota3, nota4, media;
    int i;

    printf ("Insira a primeira nota: ");
    scanf ("%f", &nota1);
    printf ("Insira a segunda nota: ");
    scanf ("%f", &nota2);
    printf ("Insira a terceira nota: ");
    scanf ("%f", &nota3);
    printf ("Insira a quarta nota: ");
    scanf ("%f", &nota4);

    media = (nota1 + nota2 + nota3 + nota4) / 4;

    if (media > 7) {
        printf ("Você foi aprovado com média: %f", media);
    } else {
        printf ("Você foi reprovado com média: %f", media);
    }
    
    return media;

}