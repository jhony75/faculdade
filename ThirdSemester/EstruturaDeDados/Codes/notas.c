// Programa que cria um arquivo, lê 4 notas (float, stdin) e escreve em um arquivo (uma nota por linha, sem texto).

#include<stdio.h>
#include<string.h>

FILE *arq;

int main (void) {

    float notas[4];
    int i = 0;

    arq = fopen("OlaMundo", "w");

    for ( i = 0; i < 4; i++) {
        
        fputs("Insira uma nota: ", stdout);
        scanf("%f", &notas[i]);
        fprintf(arq, "%f\n", notas[i]);

    }    

    fclose(arq);

}