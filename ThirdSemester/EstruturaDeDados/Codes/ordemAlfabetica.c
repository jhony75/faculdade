#include<stdio.h>
#include<string.h>

int main () {

    char primeiroNome[100], segundoNome[200];
    int i;

    fputs("Digite o primeiro nome: ", stdout);
    fgets(primeiroNome, 100, stdin);
    fputs("Digite o segundo nome: ", stdout);
    fgets(segundoNome, 100, stdin);

    i = strcmp(primeiroNome, segundoNome);

    if (i == -1) {

        printf("%s", primeiroNome);
        printf("%s", segundoNome);

    } else {
        printf("%s",segundoNome);
        printf("%s", primeiroNome);
    }

    return 0;
}