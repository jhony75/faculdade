#include <stdio.h>

//Protótipo da função

float encontrarMaior (int quantidade);

int main (void) {

    float maior;
    int tam;

    printf ("Insira o total de números a ser comparado: ");
    scanf ("%d", &tam);
    maior = encontrarMaior(tam);
    printf ("Maior valor = %f\n", maior);
    return 0;

}

float encontrarMaior (int quantidade) {

    float valor, maior;
    int i;

    for ( i = 0; i < quantidade; i++) {

        printf ("Informe um valor: ");
        scanf ("%f", &valor);

        if ( (i == 0) || ( valor > maior ) ) {

            maior = valor;

        }

    }

    return maior;

}