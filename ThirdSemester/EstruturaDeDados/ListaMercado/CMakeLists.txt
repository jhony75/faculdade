cmake_minimum_required(VERSION 3.14)
project(ListaMercado C)

set(CMAKE_C_STANDARD 11)

add_executable(ListaMercado main.c)

set(ENV{TERM} "rxvt-unicode-256color")