// Faça um programa que lê 50 valores inteiros entre 0 e 9 e conte quantas vezes cada número foi digitado (Com função e vetor).
// Jônatas Lima de Medeiros - Engenharia de Computação.
// RA: 199460418

#include <stdio.h>

//Função que lê os valores inseridos em um vetor por um usuario e imprime a quantidade de vezes que cada um foi digitado.
int contador(int valores[], int tam);

int main (void) {

    int valores[50], tam;
    tam = 50;
    contador(valores,tam); //Chama a função contador
    return 0;

}

int contador(int valores[], int tam) {

    int totais[10], i = 0;

    for (i = 0; i < 10; i++) { //Posição 0 = valor igual 0 e assim por diante.
        totais[i] = 0;
    }

    for (i = 0; i < tam; i++) { //Le os valores e conta quantas vezes eles foram digitados

        printf ("(%d) Insira um valor inteiro entre 0 e 9: ", i+1);
        scanf ("%d", &valores[i]);

        if (valores[i] > 9 || valores[i] < 0) {

            i--;
            printf ("ERRO: Valor errado inserido. ");

        } else if (valores[i] == 0) {

            totais[0]++;

        } else if (valores[i] == 1) {

            totais[1]++;

        }  else if (valores[i] == 2) {

            totais[2]++;

        }  else if (valores[i] == 3) {

            totais[3]++;

        }  else if (valores[i] == 4) {

            totais[4]++;

        }  else if (valores[i] == 5) {

            totais[5]++;

        }  else if (valores[i] == 6) {

            totais[6]++;

        }  else if (valores[i] == 7) {

            totais[7]++;

        }  else if (valores[i] == 8) {

            totais[8]++;

        } else if (valores[i] == 9) {

            totais[9]++;

        }

    }

    for (i = 0; i < 10; i++ ) {

        printf ("totais[%d] = %d\n", i, totais[i]);

    }

    return 0;

}