#include<stdio.h>
#include<string.h>

typedef struct noLista{
	int info;
	struct noLista *proximo;
	struct noLista *pesquisa;
} Elemento;

Elemento* criarNovo();
Elemento* inserirInicio(Elemento *Lista, Elemento *novo);
Elemento* inserirFinal(Elemento *Lista, Elemento *novo);
Elemento* inserirOrdem(Elemento *Lista, Elemento *novo);
Elemento* pesquisa(Elemento *Lista, int pesquisa);
Elemento* removerInicio(Elemento *Lista);
Elemento* removerFinal(Elemento *Lista);
Elemento* removerElemento(Elemento *Lista, int pesquisa);
void imprimirLista(Elemento *Lista);

main (){
	Elemento *Lista=NULL, *novo, *pesquisa;
	int i, valor, opcao;
	
	do{
		system("cls");
		printf("Escolha uma das opcoes a seguir: \n");
		printf("1 - Inserir elemento no inicio\n");
		printf("2 - Inserir elemento no final\n");
		printf("3 - Inserir elemento em ordem\n");
		printf("4 - pesquisa elemento\n");
		printf("5 - Remover elemento do inicio\n");
		printf("6 - Remover elemento do final\n");
		printf("7 - Remover elemento\n");
		printf("8 - Imprimir a lista\n");
		printf("0 - Sair\n");
		printf("Digite a opcao desejada: ");
		scanf("%d", &opcao);
		
		system("cls");
		switch(opcao){
			case 1:
				printf("Inserindo no inicio\n");
				novo = criarNovo();
				Lista = inserirInicio(Lista,novo);
				break;
			case 2:
				printf("Inserindo no final\n");
				novo = criarNovo();
				Lista = inserirFinal(Lista,novo);
				break;
			case 3:
				printf("Inserindo em ordem\n");
				novo = criarNovo();
				Lista = inserirOrdem(Lista,novo);
				break;
			case 4:
				printf("pesquisa\n\n");
				printf("Informe o valor que deseja pesquisa: ");
				scanf("%d", &valor);
				pesquisa = pesquisa(Lista,valor);
				if(pesquisa != NULL){
					printf("Dados encontrados\n");
					printf("Valor: %d\n", pesquisa->info);
				} else {
					printf("Dados nao encontrados\n");
				}
				break;
			case 5:
				printf("Remover do inicio\n");
				Lista = removerInicio(Lista);
				break;
			case 6:
				printf("Remover do final\n");
				Lista = removerFinal(Lista);
				break;
			case 7:
				printf("Remover elemento\n\n");
				printf("Informe o valor que deseja remover: ");
				scanf("%d", &valor);
				Lista = removerElemento(Lista,valor);
				break;
			case 8:
				printf("Impressao da lista\n");
				imprimirLista(Lista);
				break;
			case 0:
				printf("Encerrando\n\n");
				break;
			default:
				printf("Opcao invalida\n\n");
				break;
		}
		system("pause");		
	}while(opcao != 0);
	
}

Elemento* criarNovo(){
	Elemento *novo;
	
	novo = (Elemento*) malloc(sizeof(Elemento));
	printf("Informe o valor: ");
	scanf("%d", &novo->info);
	novo->proximo = NULL;
	novo->pesquisa = NULL;
	
	return novo;
}


Elemento* inserirInicio(Elemento *Lista, Elemento *novo){
	novo->proximo = Lista;
	if(Lista != NULL) {
		Lista->pesquisa = novo;
	}
	
	Lista = novo;
	return novo;
}

Elemento* inserirFinal(Elemento *Lista, Elemento *novo){
	Elemento *ultimo = Lista;
	
	if(Lista == NULL){
		Lista = novo;
	} else {
		while(ultimo->proximo != NULL){
			ultimo = ultimo->proximo;
		}
		ultimo->proximo = novo;
		novo->pesquisa = ultimo;
	}
	return Lista;
}


Elemento* inserirOrdem(Elemento *Lista, Elemento *novo){
	Elemento *aux = Lista, *pesquisarior;
	
	while(aux!=NULL && novo->info > aux->info){
		aux = aux->proximo;
	}
	if (aux == Lista) {
		novo->proximo = aux;
		if(aux!=NULL) {
			aux->pesquisa = novo;
		}
		Lista = novo;
	} else {
		if(aux!=NULL) {
			pesquisarior = aux->pesquisa;
		} else {
			pesquisarior = Lista;
			while (pesquisarior->proximo != aux){
				pesquisarior = pesquisarior->proximo;
			}
		}
		novo->proximo = aux;
		if(aux!=NULL) {
			aux->pesquisa = novo;
		}
		pesquisarior->proximo = novo;
		novo->pesquisa = pesquisarior;
	}
	return Lista;
}

Elemento* pesquisa(Elemento *Lista, int pesquisa){
	Elemento *aux;
	
	aux = Lista;
	while(aux!=NULL && aux->info != pesquisa){
		aux = aux->proximo;
	}
	return aux;
}

Elemento* removerInicio(Elemento *Lista){
	Elemento *remover;
	
	if (Lista != NULL) {
		remover = Lista;
		Lista = Lista->proximo;
		free(remover);
	}
	return Lista;
}

Elemento* removerFinal(Elemento *Lista){
	Elemento *ultimo, *penultimo=NULL;
	
	if(Lista != NULL) {
		ultimo = Lista;
		while(ultimo->proximo != NULL) {
			ultimo = ultimo->proximo;
		}
		if (Lista != ultimo) {
			penultimo = Lista;
			while(penultimo->proximo != ultimo){
				penultimo = penultimo->proximo;
			}
			penultimo->proximo = NULL;
		} else {
			Lista = NULL;
		}
		free(ultimo);
	}
	return Lista;
}

Elemento* removerElemento(Elemento *Lista, int pesquisa){
	Elemento *remover, *pesquisarior;
	
	if(Lista !=NULL){
		remover = pesquisa(Lista, pesquisa);
		if (remover != NULL){
			if(remover == Lista) {
				Lista = Lista->proximo;
			} else{
				pesquisarior = Lista;
				while(pesquisarior->proximo != remover){
					pesquisarior = pesquisarior->proximo;
				}
				pesquisarior->proximo = remover->proximo;
			}
			free(remover);
		} else {
			printf("Elemento nao encontrado\n");
		}
	} else {
		printf("Nao ha elementos a remover\n");
	}
	return Lista;
}


void imprimirLista(Elemento *Lista){
	Elemento *aux;
	aux = Lista;
	while(aux!=NULL){
		printf("%d\n",aux->info);
		aux = aux->proximo;
	}
	
}
